;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname bsl-test) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
(require "../main.rkt")

(define/trace (f lst)
  (cond [(empty? lst) 0]
        [else (+ (first lst) (f (rest lst)))]))

(f (list 1 2 3))


(define/trace (h n)
  (if (zero? n) 0 (g (sub1 n))))
(define/trace (g n)
  (if (zero? n) 0 (h (sub1 n))))

(h 3)